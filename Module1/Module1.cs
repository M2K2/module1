﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 oh = new Module1();

            int a = 1;
            int b = 2;
            int[] array = oh.SwapItems(a, b);
            Console.WriteLine(string.Join(", ", array));

            int[] otherarray = { 20, 10, 1 };
            int min = oh.GetMinimumValue(otherarray);
            Console.WriteLine(min);
        }


        public int[] SwapItems(int a, int b)
        {
            a += b;
            b = a - b;
            a -= b;

            int[] vs = { a, b };

            return vs;
        }

        public int GetMinimumValue(int[] input)
        {
            int MinValue = input[0];
            for (int i = 0; i < input.Length; i++)
            {
                if (MinValue > input[i])
                    MinValue = input[i];
            }
            return MinValue;
        }
     }
    }
